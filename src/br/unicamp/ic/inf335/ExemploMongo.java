package br.unicamp.ic.inf335;

import org.bson.Document;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public class ExemploMongo {
	public static void main(String[] args) {
		String url = "mongodb://localhost:27016";
		String dbase = "inf335_trabalho6";

		try {
			MongoDatabase database = conectar(url, dbase);

			System.out.println("(i) imprime a lista original de produto");
			listaProdutos(database);
			System.out.println("\n");

			System.out.println("(ii) insere um novo produto");
			Produto produto = insereProduto(database, new Produto(2, "Teste Cadastro", "Teste JAVA + MongoDB", 80.0, "NOVO"));
			System.out.println("\n");

			System.out.println("(iii) lista com o novo produto");
			listaProdutos(database);
			System.out.println("\n");

			System.out.println("(iv) altera o valor do produto inserido");
			alteraValorProduto(database, produto.getId(), 120.00);
			System.out.println("\n");

			System.out.println("(v) lista com valor do produto alterado");
			listaProdutos(database);
			System.out.println("\n");

			System.out.println("(vi) apaga o produto que foi inserido");
			apagaProduto(database, produto.getId());
			System.out.println("\n");

			System.out.println("(vii) lista novamente de modo que seja igual a lista original");
			listaProdutos(database);
			System.out.println("\n");
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
		}
	}

	public static MongoDatabase conectar(String url, String database) {
		MongoClient client = MongoClients.create(url);
		return client.getDatabase(database);
	}

	public static void listaProdutos(MongoDatabase database) {
		Iterable<Document> dProdutos =  database.getCollection("produtos").find();
		Produto produto;

		for (Document dProduto : dProdutos) {
			produto = new Produto(
					dProduto.getInteger("id"), 
					dProduto.getString("nome"), 
					dProduto.getString("descricao"),
					dProduto.getDouble("valor"), 
					dProduto.getString("estado")
			);

			System.out.println(produto.toString());
		}
	}

	public static Produto insereProduto(MongoDatabase database, Produto produto) {
		Document doc = Document.parse(produto.toJson());
		database.getCollection("produtos").insertOne(doc);

		return produto;
	}

	public static void alteraValorProduto(MongoDatabase database, Integer idProduto, Double novoValor) {
		Document query = Document.parse("{ \"id\": " + idProduto + " }");
		Document update = Document.parse("{ \"$set\": { \"valor\": " + novoValor + " } }");

		database.getCollection("produtos").updateMany(query, update);
	}

	public static void apagaProduto(MongoDatabase database, Integer idProduto) {
		Document query = Document.parse("{\"id\": " + idProduto + "}");

		database.getCollection("produtos").deleteMany(query);
	}
}
