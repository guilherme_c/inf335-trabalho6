package br.unicamp.ic.inf335;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ExemploMysql {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3305/inf335_trabalho6";
		String usuario = "root";
		String senha = "secret-pw";

		Connection conn = null;
		try {
			conn = conectar(url, usuario, senha);

			System.out.println("(i) imprime a lista original de produto");
			listaProdutos(conn);
			System.out.println("\n");

			System.out.println("(ii) insere um novo produto");
			Produto produto = insereProduto(conn, new Produto("Teste Cadastro", "Teste JAVA + MySQL com JDBC", 80.0, "NOVO"));
			System.out.println("\n");

			System.out.println("(iii) lista com o novo produto");
			listaProdutos(conn);
			System.out.println("\n");

			System.out.println("(iv) altera o valor do produto inserido");
			alteraValorProduto(conn, produto.getId(), 120.00);
			System.out.println("\n");

			System.out.println("(v) lista com valor do produto alterado");
			listaProdutos(conn);
			System.out.println("\n");

			System.out.println("(vi) apaga o produto que foi inserido");
			apagaProduto(conn, produto.getId());
			System.out.println("\n");

			System.out.println("(vii) lista novamente de modo que seja igual a lista original");
			listaProdutos(conn);
			System.out.println("\n");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// (viii) fecha conexão.
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static Connection conectar(String url, String usuario, String senha) {
		Connection conn = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection(url, usuario, senha);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return conn;
	}

	public static void listaProdutos(Connection conn) throws SQLException {
		PreparedStatement stmt = null;

		try {
			stmt = conn.prepareStatement("SELECT id, nome, descricao, valor, estado FROM produtos;");
			ResultSet rs = stmt.executeQuery();

			Produto produto;
			while (rs.next()) {
				produto = new Produto(
						rs.getInt("id"), 
						rs.getString("nome"), 
						rs.getString("descricao"),
						rs.getDouble("valor"), 
						rs.getString("estado")
				);

				System.out.println(produto.toString());
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			stmt.close();
		}
	}

	public static Produto insereProduto(Connection conn, Produto produto) throws SQLException {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("INSERT INTO produtos (nome, descricao, valor, estado) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, produto.getNome());
			stmt.setString(2, produto.getDescricao());
			stmt.setDouble(3, produto.getValor());
			stmt.setString(4, produto.getEstado());

			stmt.executeUpdate();

			ResultSet keys = stmt.getGeneratedKeys();
			keys.next();  

			produto.setId(keys.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			stmt.close();
		}

		return produto;
	}

	public static boolean alteraValorProduto(Connection conn, Integer idProduto, Double novoValor) throws SQLException {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("UPDATE produtos SET valor = ? WHERE id = ?");
			stmt.setDouble(1, novoValor);
			stmt.setInt(2, idProduto);

			return stmt.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			stmt.close();
		}
	}

	public static boolean apagaProduto(Connection conn, Integer idProduto) throws SQLException {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("DELETE FROM produtos WHERE id = ?");
			stmt.setInt(1, idProduto);

			return stmt.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			stmt.close();
		}
	}
}
